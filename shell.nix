{ pkgs ? import <nixpkgs> {}}:
let pjson = pkgs.haskellPackages.callCabal2nix "pjson" ./. {};
in
pkgs.haskellPackages.shellFor {
  packages = p: [ pjson ];
}