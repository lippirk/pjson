{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Test.Hspec
import           PJson
import           Data.Text                     as T
import           Data.List                     as L
import           Data.Text.IO                  as TIO
import           Control.Monad                  ( mapM_ )

data TestCase = TestCase
  { _title    :: !Text
  , _input    :: !Text
  , _expected :: !Text
  } deriving (Eq, Show)

main :: IO ()
main = do
  mtestCases' <- parseTestCases <$> TIO.readFile "test-cases"
  case mtestCases' of
    Nothing         -> error "Could not parse test cases"
    Just testCases' -> hspec $ mapM_ toSpec testCases'

toSpec :: TestCase -> Spec
toSpec (TestCase title' input' expected') =
  it (T.unpack title') $ pretty input' `shouldBe` expected'

parseTestCases :: Text -> Maybe [TestCase]
parseTestCases = go . T.lines
 where
  go []        = Just []
  go ("" : xs) = go xs
  go xs        = do
    (title'   , rest'  ) <- parseTitle xs
    (input'   , rest'' ) <- parseUntil "===>" rest'
    (expected', rest''') <- parseUntil "####" rest''
    fmap
      (TestCase { _title = title', _input = input', _expected = expected' } :)
      (go rest''')
  parseTitle (t' : rest') =
    let (hashes', title') = T.splitAt 5 t'
    in  if hashes' == "#### " then Just (title', rest') else Nothing
  parseUntil token' xs' = case L.span (/= token') xs' of
    (lines', token' : rest') -> Just (T.unlines lines', rest')
    _                        -> Nothing
