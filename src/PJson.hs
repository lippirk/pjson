{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiWayIf #-}
module PJson
  ( pjson
  , pretty
  )
where

import           Data.Text                     as T
import qualified Data.Text.IO                  as TIO
import           Data.List                     as L
import           Control.Monad.Trans.State.Strict
import           Control.Monad                  ( unless
                                                , when
                                                )
type S = State ParseState

pjson :: IO ()
pjson = TIO.putStrLn . pretty =<< TIO.getContents

pretty :: Text -> Text
pretty t = T.snoc
  (_pretty . snd $ runState
    go
    (ParseState { _tokens       = []
                , _pretty       = ""
                , _ugly         = t
                , _insideQuotes = False
                , _currCol      = 0
                }
    )
  )
  '\n'
 where
  go = do
    mchar'        <- takeNextUglyChar
    insideQuotes' <- insideQuotes
    case mchar' of
      Nothing    -> return () -- nothing left to parse
      Just char' -> do
        if insideQuotes'
          then appendChar char'
          else case char' of
            '\n' -> return ()
            ' '  -> return ()
            '\t' -> return ()
            '{'  -> lookaheadForEmptyOr
              (appendChar '{' >> appendOnNewLine "  ")
              '{'
              '}'
            '[' -> lookaheadForEmptyOr (appendText "[ ") '[' ']'
            ':' -> appendText ": "
            ',' -> appendOnNewLine ", "
            ']' -> do
              appendText " ]"
              insideArray' <- insideArray
              when insideArray' setColToLastToken
              dropToken
            '}' -> do
              setColToLastToken
              appendOnNewLine "}"
              dropToken
            _ -> appendChar char'
        go

   where
    lookaheadForEmptyOr default' openParens' closeParens' = do
      nextNonWsChars' <- T.dropWhile (`elem` whitespaceChars) . _ugly <$> get
      if
        | -- Malformed json, but append anyway
          T.null nextNonWsChars' -> appendChar openParens'
        | -- the object / array was empty
          T.head nextNonWsChars' == closeParens' -> do
          appendText . T.pack $ [openParens', ' ', closeParens']
          modify' $ \s' -> s' { _ugly = T.tail nextNonWsChars' }
          dropToken
          setColToLastToken
        | otherwise -> default'

    setColToLastToken = do
      s' <- get
      case snd <$> safeHead (_tokens s') of
        Nothing   -> return ()
        Just col' -> put $ s' { _currCol = col' }

    lastCol     = (\s' -> snd <$> safeHead (_tokens s')) <$> get

    insideArray = do
      tokens' <- _tokens <$> get
      return $ case safeHead tokens' of
        Just ('[', _) -> True
        _             -> False

    appendOnNewLine t' = do
      mcol' <- lastCol
      case mcol' of
        Nothing   -> appendText $ "\n  " <> t'
        Just col' -> appendText $ "\n" <> T.replicate col' " " <> t'

    appendChar c = modify' $ \currState' ->
      let currAcc' = _pretty currState'
          currCol' = _currCol currState'
      in  currState' { _pretty  = currAcc' `T.snoc` c
                     , _currCol = if c == '\n' then 0 else currCol' + 1
                     }

    appendText t' = modify' $ \currState' ->
      let currAcc          = _pretty currState'
          currCol'         = _currCol currState'
          splitByNewlines' = T.split (== '\n') t'
      in  currState'
            { _pretty  = currAcc <> t'
            , _currCol = if L.length splitByNewlines' == 1
                           then currCol' + T.length t'
                           else T.length (L.last splitByNewlines')
            }

    dropToken = modify' $ \s' ->
      let currTokens' = _tokens s'
      in  case currTokens' of
            [] -> s'
            _  -> s' { _tokens = L.tail currTokens' }

whitespaceChars :: [Char]
whitespaceChars = ['\t', ' ', '\n']

insideQuotes :: S Bool
insideQuotes = _insideQuotes <$> get

takeNextUglyChar :: S (Maybe Char)
takeNextUglyChar = do
  state' <- get
  case T.uncons (_ugly state') of
    Nothing             -> return Nothing
    Just (next', rest') -> do
      put $ state' { _ugly = rest' }
      adjustInsideQuotes next'
      adjustTokens next'
      return $ Just next'
 where
  adjustInsideQuotes '\"' = modify' $ \s ->
    let currentlyInsideQuotes' = _insideQuotes s
    in  s { _insideQuotes = not currentlyInsideQuotes' }
  adjustInsideQuotes _ = return ()

  adjustTokens c = do
    currCol'      <- _currCol <$> get
    insideQuotes' <- insideQuotes
    unless insideQuotes' $ case c of
      '[' -> pushToken currCol'
      '{' -> pushToken currCol'
      _   -> return ()
   where
    pushToken col' = modify' $ \state' ->
      let currTokens' = _tokens state'
      in  state' { _tokens = (c, col') : currTokens' }

type Column = Int

-- | One of: '[','{'
type Token = Char

data ParseState = ParseState
  { _tokens :: ![(Token, Column)]
  -- | Stack of the last token, and where it was placed
  , _pretty    :: !Text
  -- | TODO builder should be more efficient
  , _ugly      :: !Text

  , _insideQuotes :: !Bool
  -- | When inside quotes we ignore the container, and just print
  , _currCol :: !Column
  -- | Cursor telling us where to print
  } deriving (Eq, Show)

safeHead :: [a] -> Maybe a
safeHead []      = Nothing
safeHead (x : _) = Just x
